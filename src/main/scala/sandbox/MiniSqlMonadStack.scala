package stack

import pprint.pprintln
import cats.data.{Chain, EitherT, ReaderWriterState}
import cats.implicits._


sealed trait RawName {
  def value: String
}
final case class RawTableName(value: String) extends RawName
final case class RawColumnName(value: String) extends RawName

sealed trait ResolvedName
final case class ResolvedTableName(value: String) extends ResolvedName
final case class ResolvedTableAlias(value: String) extends ResolvedName
final case class ResolvedColumnName(value: String) extends ResolvedName

sealed trait Query[R]
case class QuerySelect[R](qs: Select[R]) extends Query[R]
case class QueryWith[R](ctes: List[CTE[R]], q: Query[R]) extends Query[R]

case class CTE[R](alias: String, q: Query[R])

case class Column[R](col: R, alias: Option[String])
case class Select[R](select: List[Column[R]], from: Option[List[R]])

case class Catalog(c: Map[String, List[String]]) {
  def relationIsInCatalog(rt: RawName): Boolean = c.contains(rt.value)
  def maybeGetRelation(r: RawName): Option[(String, List[String])] =
    c.get(r.value).map(cs => (r.value, cs))
}
case class Resolver private(
  // TODO perhaps Resolver has a stack of Resolvers?
  t: Map[String, List[String]], // temps and ctes in scope
  r: List[String], // relations in scope
  s: List[String] // projection in current scope's SELECT clause
) {
  def addRelationToScope(k: String, v: List[String]): Resolver = this.copy(t = t.updated(k, v), r = k :: r)
  def relationIsAlias(rt: RawName): Boolean = t.contains(rt.value)
  def addAliasToScope(rt: RawName): Resolver = this.copy(r = rt.value :: r)

  def addColumnToProjection(rc: String): Resolver = this.copy(s = rc :: s)
  def columnIsInScope(rc: RawName): Boolean =
    r.exists(rr => t.get(rr).map(cols => cols.contains(rc.value)).getOrElse(false))

  def addCTE(alias: String): Resolver = this.copy(t = t.updated(alias, s))
  def resetRelationScope(): Resolver = this.copy(r = List.empty, s = List.empty)
}
object Resolver {
  def apply() = new Resolver(Map.empty, List.empty, List.empty)
}

object MonadSqlState extends App {
  type Log = Chain[String]
  type RState[A] = ReaderWriterState[Catalog, Log, Resolver, A]
  type RSER[X] = EitherT[RState, ResolutionError, X]

  case class ResolutionError(value: RawName)

  def resolveOneRelation(tableName: RawName): RState[Either[ResolutionError, ResolvedName]] = ReaderWriterState { (c, s) =>
    if (s.relationIsAlias(tableName))
      (Chain(s"Table ${tableName.value} is an alias in scope"),
       s.addAliasToScope(tableName),
       Right(ResolvedTableAlias(tableName.value)))
    else
      c.maybeGetRelation(tableName) match {
        case Some((tn, cols)) =>
          (Chain(s"Table ${tn} was in catalog"),
           s.addRelationToScope(tn, cols),
           Right(ResolvedTableName(tn)))
        case None =>
          (Chain(s"Unresolved table ${tableName.value}"),
           s,
           Left(ResolutionError(tableName)))
      }
    }

  def resolveOneCol(col: Column[RawName]): RState[Either[ResolutionError, Column[ResolvedName]]] = ReaderWriterState { (c, s) =>
    if (s.columnIsInScope(col.col))
      (Chain(s"Resolved Column ${col.col.value}"),
       s.addColumnToProjection(col.alias.getOrElse(col.col.value)),
       Right(Column(ResolvedColumnName(col.alias.getOrElse(col.col.value)), col.alias)))
    else
      (Chain(s"""Column ${col.col.value} was not resolvable with relations: ${s.r.mkString(",")}"""),
       s, Left(ResolutionError(col.col)))
  }

  def resolveFrom(tables: List[RawName]): EitherT[RState, ResolutionError, List[ResolvedName]] =
    EitherT(tables.traverse(resolveOneRelation).map(_.sequence))

  def resolveSelection(cols: List[Column[RawName]]): EitherT[RState, ResolutionError, List[Column[ResolvedName]]] =
    EitherT(cols.traverse(resolveOneCol).map(_.sequence))

  def resolveSelect(s: Select[RawName]): EitherT[RState, ResolutionError, Select[ResolvedName]] = for {
    from <- s.from.map(resolveFrom).sequence
    select <- resolveSelection(s.select)
  } yield Select(select, from)

  def resolveOneCTE(cte: CTE[RawName]): EitherT[RState, ResolutionError, CTE[ResolvedName]] = for {
    query <- resolveQuery(cte.q)
    // Update state with the CTE alias and the selection columns
    _ <- EitherT.right(ReaderWriterState.modify[Catalog, Log, Resolver]{case r => r.addCTE(cte.alias)})
  } yield CTE(cte.alias, query)

  def resQueryWith(qw: QueryWith[RawName]): EitherT[RState, ResolutionError, Query[ResolvedName]] = for {
    ctes <- qw.ctes.traverse(resolveOneCTE)
    query <- resolveQuery(qw.q)
  } yield QueryWith(ctes, query)

  def resolveQuery(q: Query[RawName]): RSER[Query[ResolvedName]] = q match {
    case QuerySelect(qs) => resolveSelect(qs).map {s => QuerySelect(s)}
    case QueryWith(ctes, q) => resQueryWith(QueryWith(ctes, q))
  }

//  val q: Query[RawName] = QuerySelect(Select(List(Column(RawColumnName("a"), None)), Some(List(RawTableName("db.foo")))))
//
//  val res = resolveQuery(q).value.run(Catalog(Map("db.foo" -> List("a", "b"))), Resolver()).value
//
//  println(res)

  val qw: Query[RawName] = QueryWith(List(CTE("hasAA", QuerySelect(Select(List(Column(RawColumnName("a"), Some("aa"))), Some(List(RawTableName("db.foo"))))))),
                    QuerySelect(Select(List(Column(RawColumnName("aa"), Some("myalias"))), Some(List(RawTableName("hasAA"))))))

  val (log, finalState, rq) = resolveQuery(qw).value.run(Catalog(Map("db.foo" -> List("a", "b"))), Resolver()).value

  println("LOG -------")
  pprintln(log)

  println("FINAL STATE -------")
  pprintln(finalState)

  println("RESOLVED QUERY -------")
  pprintln(rq, height=10000)
}
