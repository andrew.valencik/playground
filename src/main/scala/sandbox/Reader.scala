package sandbox

import cats.data.Reader

object CatReader extends App {
  case class Cat(name: String, food: String)

  val catName: Reader[Cat, String] = Reader(cat => cat.name)

  val greetKitty: Reader[Cat, String] = catName.map(n => s"Hello ${n}")

  val feedKitty: Reader[Cat, String] = Reader(cat => s"Have a nice bowl of ${cat.food}")

  val greetAndFeed = for {
    greet <- greetKitty
    feed <- feedKitty
  } yield s"${greet}. ${feed}."

  val result = greetAndFeed.run(Cat("Hops", "treats"))
  println(result)

  val feedAndGreet: Reader[Cat, String] = feedKitty.flatMap{
    f => greetKitty.map{g => s"${f}. ${g}."}
  }
  println(feedAndGreet(Cat("Nigel", "literally anything")))
}
