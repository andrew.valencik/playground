package sandbox

import scala.annotation.tailrec

import cats.data.Writer
import cats.effect._
import cats.instances.vector._ // for Monoid
import cats.syntax.writer._ // for Writer
import cats.syntax.applicative._ // for pure


object FactorialWriter extends IOApp {
  type Logged[A] = Writer[Vector[String], A]

  def factorial(n: Int): Int = {
    @tailrec
    def inner(acc: Int, n: Int): Int =  if (n == 0) acc else inner(acc * n, n-1)
    inner(1, n)
  }

  def factorialLog(n: Int): Logged[Int] =
    for {
      _ <- Vector(s"factorialLog called with n = ${n}").tell
      r <- factorial(n).pure[Logged]
      _ <- Vector(s"factorial(${n}) = ${r}").tell
    } yield r

  def putStrlLn(value: String) = IO(println(value))
  val readLn = IO(scala.io.StdIn.readLine)
  val readInt = IO(scala.io.StdIn.readInt)

  def run(args: List[String]): IO[ExitCode] = for {
      _ <- putStrlLn("What's your name?")
      n <- readLn
      _ <- putStrlLn(s"Hello, $n!")
      _ <- putStrlLn("What number?")
      x <- readInt
      r <- IO(factorialLog(x))
      _ <- putStrlLn(r.written.mkString("\n"))
  } yield ExitCode.Success

}
