package TraverseTree

import cats.{Applicative, Eq, Eval, Traverse}
import cats.data.State
import cats.implicits._

case class RawName(value: String)
case class RawRelation(value: String)
case class ResolvedName(value: String)

final case class Column[I, R](info: I, value: R)
object Column {
  implicit def eqColumn[I: Eq, R: Eq]: Eq[Column[I, R]] = Eq.fromUniversalEquals
  implicit def columnInstances[I]: Traverse[Column[I, ?]] = new Traverse[Column[I, ?]] {
    override def map[A, B](fa: Column[I, A])(f: A => B): Column[I, B] = fa.copy(value = f(fa.value))
    def foldLeft[A, B](fa: Column[I, A], b: B)(f: (B, A) => B): B = f(b, fa.value)
    def foldRight[A, B](fa: Column[I, A], lb: Eval[B])(f: (A, Eval[B]) => Eval[B]): Eval[B] = Eval.defer(f(fa.value, lb))
    def traverse[G[_], A, B](fa: Column[I, A])(f: A => G[B])(implicit G: Applicative[G]): G[Column[I, B]] =
      Applicative[G].map(f(fa.value))(b => fa.map(_ => b))
  }
}

final case class Relation[I, R](info: I, value: R)
object Relation {
  implicit def eqRelation[I: Eq, R: Eq]: Eq[Relation[I, R]] = Eq.fromUniversalEquals
  implicit def columnInstances[I]: Traverse[Relation[I, ?]] = new Traverse[Relation[I, ?]] {
    override def map[A, B](fa: Relation[I, A])(f: A => B): Relation[I, B] = fa.copy(value = f(fa.value))
    def foldLeft[A, B](fa: Relation[I, A], b: B)(f: (B, A) => B): B = f(b, fa.value)
    def foldRight[A, B](fa: Relation[I, A], lb: Eval[B])(f: (A, Eval[B]) => Eval[B]): Eval[B] = Eval.defer(f(fa.value, lb))
    def traverse[G[_], A, B](fa: Relation[I, A])(f: A => G[B])(implicit G: Applicative[G]): G[Relation[I, B]] =
      Applicative[G].map(f(fa.value))(b => fa.map(_ => b))
  }
}

sealed trait Node[I, R]
final case class ColumnWrap[I, R](info: I, col: Column[I, R]) extends Node[I, R]
object ColumnWrap {
  implicit def eqColumnWrap[I: Eq, R: Eq]: Eq[ColumnWrap[I, R]] = Eq.fromUniversalEquals
  implicit def columnWrapInstances[I]: Traverse[ColumnWrap[I, ?]] = new Traverse[ColumnWrap[I, ?]] {
    override def map[A, B](fa: ColumnWrap[I, A])(f: A => B): ColumnWrap[I, B] = fa.copy(col = fa.col.map(f))
    def foldLeft[A, B](fa: ColumnWrap[I, A], b: B)(f: (B, A) => B): B = f(b, fa.col.value)
    def foldRight[A, B](fa: ColumnWrap[I, A], lb: Eval[B])(f: (A, Eval[B]) => Eval[B]): Eval[B] = Eval.defer(f(fa.col.value, lb))
    def traverse[G[_], A, B](fa: ColumnWrap[I, A])(f: A => G[B])(implicit G: Applicative[G]): G[ColumnWrap[I, B]] =
      Applicative[G].map(f(fa.col.value))(b => fa.map(_ => b))
  }
}

final case class ManyCols[I, R](info: I, cols: List[Column[I, R]]) extends Node[I, R]
object ManyCols {
  implicit def eqManyCols[I: Eq, R: Eq]: Eq[ManyCols[I, R]] = Eq.fromUniversalEquals
  implicit def columnWrapInstances[I]: Traverse[ManyCols[I, ?]] = new Traverse[ManyCols[I, ?]] {
    override def map[A, B](fa: ManyCols[I, A])(f: A => B): ManyCols[I, B] = fa.copy(cols = fa.cols.map(_.map(f)))
    def foldLeft[A, B](fa: ManyCols[I, A], b: B)(f: (B, A) => B): B = fa.cols.foldLeft(b){(bb, c) => c.foldLeft(bb)(f)}
    def foldRight[A, B](fa: ManyCols[I, A], lb: Eval[B])(f: (A, Eval[B]) => Eval[B]): Eval[B] =
      Eval.defer(fa.cols.foldRight(lb){(c, bb) => c.foldRight(Eval.defer(bb))(f)})
    def traverse[G[_], A, B](fa: ManyCols[I, A])(f: A => G[B])(implicit G: Applicative[G]): G[ManyCols[I, B]] =
      Applicative[G].map(fa.cols.traverse(_.traverse(f)))(ManyCols(fa.info, _))
  }
}

final case class From[I, R](info: I, rels: List[Relation[I, R]]) extends Node[I, R]
object From {
  implicit def eqFrom[I: Eq, R: Eq]: Eq[From[I, R]] = Eq.fromUniversalEquals
  implicit def fromInstances[I]: Traverse[From[I, ?]] = new Traverse[From[I, ?]] {
    override def map[A, B](fa: From[I, A])(f: A => B): From[I, B] = fa.copy(rels = fa.rels.map(_.map(f)))
    def foldLeft[A, B](fa: From[I, A], b: B)(f: (B, A) => B): B = fa.rels.foldLeft(b){(bb, c) => c.foldLeft(bb)(f)}
    def foldRight[A, B](fa: From[I, A], lb: Eval[B])(f: (A, Eval[B]) => Eval[B]): Eval[B] =
      Eval.defer(fa.rels.foldRight(lb){(c, bb) => c.foldRight(Eval.defer(bb))(f)})
    def traverse[G[_], A, B](fa: From[I, A])(f: A => G[B])(implicit G: Applicative[G]): G[From[I, B]] =
      Applicative[G].map(fa.rels.traverse(_.traverse(f)))(From(fa.info, _))
  }
}

final case class Query[I, R](info: I, select: ManyCols[I, R], from: From[I, R]) extends Node[I, R]
object Query {
  implicit def eqQuery[I: Eq, R: Eq]: Eq[Query[I, R]] = Eq.fromUniversalEquals
  implicit def queryInstances[I]: Traverse[Query[I, ?]] = new Traverse[Query[I, ?]] {
    override def map[A, B](fa: Query[I, A])(f: A => B): Query[I, B] = fa.copy(from = fa.from.map(f), select = fa.select.map(f))
    def foldLeft[A, B](fa: Query[I, A], b: B)(f: (B, A) => B): B = fa.select.foldLeft(fa.from.foldLeft(b)(f))(f)
    def foldRight[A, B](fa: Query[I, A], lb: Eval[B])(f: (A, Eval[B]) => Eval[B]): Eval[B] =
      Eval.defer(fa.from.foldRight(fa.select.foldRight(lb)(f))(f))
    def traverse[G[_], A, B](fa: Query[I, A])(f: A => G[B])(implicit G: Applicative[G]): G[Query[I, B]] =
      Applicative[G].map2(fa.select.traverse(f), fa.from.traverse(f))(Query(fa.info, _, _))
  }
}

object Main extends App {
  def resolve(n: RawName): State[String, ResolvedName] = State { acc =>
    (acc + s"\nresolved ${n.value}", ResolvedName(n.value))
  }

//  def resolveNode[I](node: Node[I, RawName]): State[String, Node[I, ResolvedName]] =
//    node match {
//      case c: ColumnWrap[I, RawName] => c.traverse(resolve).widen
//      case c: ManyCols[I, RawName] => c.traverse(resolve).widen
//    }

  val myNode = ManyCols(0, List(Column(1, RawName("Hi")), Column(2, RawName("Yo")),  Column(3, RawName("Three"))))

  println(myNode.toList)

}
