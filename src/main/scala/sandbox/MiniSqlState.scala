package sandbox

import cats.data.State
import cats.implicits._


sealed trait RawName {
  def value: String
}
final case class RawTableName(value: String) extends RawName
final case class RawColumnName(value: String) extends RawName

sealed trait ResolvedName
final case class ResolvedTableName(value: String) extends ResolvedName
final case class ResolvedTableAlias(value: String) extends ResolvedName
final case class UnresolvedTableName(value: String) extends ResolvedName
final case class ResolvedColumnName(value: String) extends ResolvedName
final case class UnresolvedColumnName(value: String) extends ResolvedName

sealed trait Query[R]
case class QuerySelect[R](qs: Select[R]) extends Query[R]
case class QueryWith[R](ctes: List[CTE[R]], q: Query[R]) extends Query[R]

case class CTE[R](alias: String, q: Query[R])

case class Select[R](select: List[R], from: Option[List[R]])

case class Resolver private(
  c: Map[String, Set[String]], // immutable global database catalog
  ctes: Map[String, Set[String]], // ctes in scope
  r: Set[String], // relations in scope
  s: Set[String] // projection in current scope's SELECT clause
) {
  def relationIsInCatalog(rt: RawName): Boolean = c.contains(rt.value)
  def addRelationToScope(rt: RawName): Resolver = this.copy(r = r + rt.value)
  def relationIsAlias(rt: RawName): Boolean = ctes.contains(rt.value)
  def addAliasToScope(rt: RawName): Resolver = this.copy(r = r + rt.value)

  // TODO Can I clean up these flattens?
  lazy val columnsInScope: Set[String] = r.flatMap{rn => Set(c.get(rn), ctes.get(rn)).flatten}.flatten
  def columnIsInScope(rc: RawName): Boolean = columnsInScope(rc.value)

  def addCTE(alias: String, cols: Set[String]): Resolver = this.copy(ctes = ctes.updated(alias, cols))
  def resetRelationScope(): Resolver = this.copy(r = Set.empty, s = Set.empty)
}
object Resolver {
  def apply(c: Map[String, Set[String]]) = new Resolver(c, Map.empty, Set.empty, Set.empty)
}


object MiniSqlState extends App {
  type QueryState[R] = State[Resolver, Query[R]]
  type ResolverState[A] = State[Resolver, A]


  def getSelectionColumns[R](q: Query[R]): List[R] = q match {
    case QuerySelect(qs) => qs.select
    case QueryWith(ctes, q) => getSelectionColumns(q)
  }

  def resolveOneRelation(tableName: RawName) = State[Resolver, ResolvedName] { acc =>
    if (acc.relationIsInCatalog(tableName))
      (acc.addRelationToScope(tableName), ResolvedTableName(tableName.value))
    else if (acc.relationIsAlias(tableName))
      (acc.addAliasToScope(tableName), ResolvedTableAlias(tableName.value))
    else {
      (acc, UnresolvedTableName(tableName.value))
    }
  }

  def resolveFrom(tables: List[RawName]) = tables.traverse(resolveOneRelation)

  def resolveSelection(cols: List[RawName]) = State[Resolver, List[ResolvedName]] { acc =>
    val rcols = cols.map { col =>
      if (acc.columnIsInScope(col))
        ResolvedColumnName(col.value)
      else
        UnresolvedColumnName(col.value)
    }
    (acc, rcols)
  }

  def resolveSelect(s: Select[RawName]): ResolverState[Select[ResolvedName]] = for {
    from <- s.from.traverse(resolveFrom)
    select <- resolveSelection(s.select)
  } yield Select(select, from)

  def resolveOneCTE(cte: CTE[RawName]) = for {
    query <- resolveQuery(cte.q)
    // Update state with the CTE alias and the selection columns
    _ <- State.modify[Resolver](c => c.addCTE(cte.alias, getSelectionColumns(cte.q).map(_.value).toSet))
    // Update state to have no relations or columns in scope so we don't over resolve
    _ <- State.modify[Resolver](c => c.resetRelationScope)
  } yield CTE(cte.alias, query)

  def resQueryWith(qw: QueryWith[RawName]): QueryState[ResolvedName] = for {
    ctes <- qw.ctes.traverse(resolveOneCTE)
    query <- resolveQuery(qw.q)
  } yield QueryWith(ctes, query)

  def resolveQuery(q: Query[RawName]): QueryState[ResolvedName] = q match {
    case QuerySelect(qs) => resolveSelect(qs).map {s => QuerySelect(s)}
    case QueryWith(ctes, q) => resQueryWith(QueryWith(ctes, q))
  }

}
