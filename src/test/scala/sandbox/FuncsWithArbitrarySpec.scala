package FuncsWithArbitrary

import cats.implicits._
import org.scalatest.funsuite.AnyFunSuite
import org.typelevel.discipline.scalatest.Discipline
import org.scalacheck.{Arbitrary, Gen}
import org.scalacheck.Arbitrary.{arbitrary => getArbitrary}

import cats.laws.discipline.FunctorTests

object arbitrary {
  implicit def arbColumn[I: Arbitrary, R: Arbitrary]: Arbitrary[Column[I, R]] =
    Arbitrary(for { i <- getArbitrary[I]; r <- getArbitrary[R] } yield Column(i, r))

  implicit def arbNode[I: Arbitrary, R: Arbitrary]: Arbitrary[Node[I, R]] =
    Arbitrary(Gen.frequency((10, getArbitrary[Wrap[I, R]]), (2, getArbitrary[ManyCols[I, R]]), (1, getArbitrary[ManyNodes[I, R]])))

  implicit def arbWrap[I: Arbitrary, R: Arbitrary]: Arbitrary[Wrap[I, R]] =
    Arbitrary(for { i <- getArbitrary[I]; r <- getArbitrary[Column[I, R]] } yield Wrap(i, r))

  implicit def arbManyCols[I: Arbitrary, R: Arbitrary]: Arbitrary[ManyCols[I, R]] =
    Arbitrary(for { i <- getArbitrary[I]; r <- getArbitrary[List[Column[I, R]]] } yield ManyCols(i, r))

  implicit def arbManyNodes[I: Arbitrary, R: Arbitrary]: Arbitrary[ManyNodes[I, R]] =
    Arbitrary(for { i <- getArbitrary[I]; r <- Gen.resize(5, getArbitrary[List[Node[I, R]]]) } yield ManyNodes(i, r))
}

class TreeLawTests extends AnyFunSuite with Discipline {
  import arbitrary._

  checkAll("Column[Int, ?]", FunctorTests[Column[Int, ?]].functor[Int, Int, String])

  checkAll("Wrap[Int, ?]", FunctorTests[Wrap[Int, ?]].functor[Int, Int, String])

  checkAll("ManyCols[Int, ?]", FunctorTests[ManyCols[Int, ?]].functor[Int, Int, String])

  checkAll("Node[Int, ?]", FunctorTests[Node[Int, ?]].functor[Int, Int, String])

  checkAll("ManyNodes[Int, ?]", FunctorTests[ManyNodes[Int, ?]].functor[Int, Int, String])
}
